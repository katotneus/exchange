#!/bin/bash
pathDockerFolder="./.docker";

echo "==> 進入 .docker 資料夾";
if [ ! -d ${pathDockerFolder} ]; then
    echo "[錯誤] 沒有 .docker 資料夾";
    exit 1;
fi
cd ${pathDockerFolder};

echo "==> 執行 docker-compose"
if [ ! -f "docker-compose.yml" ]; then
    echo "[錯誤] 沒有 docker-compose.yml";
    exit 1;
fi
if [ ! -f ".env" ]; then
    echo "[錯誤] 請建立 .env 檔";
    exit 1;
fi
docker-compose up -d nginx mysql redis adminer php-worker
