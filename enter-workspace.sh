#!/bin/bash
pathDockerFolder="./.docker";

echo "==> 進入 .docker 資料夾";
if [ ! -d ${pathDockerFolder} ]; then
    echo "[錯誤] 沒有 .docker 資料夾";
    exit 1;
fi
cd ${pathDockerFolder};

echo "==> 執行 docker-compose exec workspace bash"
docker-compose exec workspace bash