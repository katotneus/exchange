# ER模型圖
- [draw.io](https://drive.google.com/file/d/0BxFiFUJX_D9tWk10Smd5QkZQblU/view?usp=sharing) [唯讀, 可討論]

# 環境需求
下列擇一即可，此專案有包含 Docker 環境
- Docker環境
    1. docker
    2. docker-compose
- 一般環境
    1. PHP 7.1 以上
    2. MySQL 5.7 以上
    3. Redis 4 以上

# 安裝-Docker開發環境
1. 下載 docker-compose 檔
    ```bash
    git submodule update
    ```
2. 修改 docker-compose 環境設定
    ```bash
    # 進入 .docker 資料夾
    cd .docker

    # 複製 env-example，並命名為 .env
    cp env-example .env

    # 編輯 .env
    vi .env
    # 以下是建議修改
    # 1. COMPOSE_PROJECT_NAME=exchange (定義產生的 docker 容器名稱前撮, ex: exchange)
    # 2. DATA_SAVE_PATH=./data (設定所有資料儲存的位置, ex: ./data)
    # 以下是可改可不改
    # 1. MYSQL_DATABASE=default (資料庫名稱)
    # 2. MYSQL_USER=default (資料庫使用者名稱)
    # 3. MYSQL_PASSWORD=secret (資料庫使用者密碼)
    # 4. MYSQL_ROOT_PASSWORD=root (資料庫ROOT使用者密碼)
    ```
3. 修改專案環境設定
    ```bash
    # 返回專案根目錄
    cd ../
    
    # 複製 .env-example，並命名為 .env
    cp .env-example .env

    # 編輯 .env
    vi .env
    # 以下是必須修改
    # 1. DB_HOST=mysql (資料庫位置，必須填 mysql)
    # 2. DB_DATABASE=default (資料庫名稱)
    # 3. DB_USERNAME=default (資料庫使用者名稱)
    # 4. DB_PASSWORD=secret (資料庫密碼)
    # 5. QUEUE_DRIVER=redis (隊列驅動使用 redis)
    # 6. QUEUE_HOST=redis (redis 的位置)
    # 7. REDIS_HOST=redis (redis 的位置)
    # 8. CACHE_DRIVER=redis
    # 9. SESSION_DRIVER=redis
    ```
4. 啟動 docker-compose
    ```bash
    ./start-server.sh
    # or
    cd .docker
    docker-compose up -d nginx mysql redis adminer php-worker
    ```
5. 進入 工作區 終端機
    ```bash
    ./enter-workspace.sh
    # or
    cd .docker
    docker-compose exec workspace bash
    ```
6. 安裝專案依賴
    ```bash
    # !在工作區終端機內
    # 切換使用者
    su laradock
    
    # 安裝依賴
    compose install

    # 等待...
    ```
7. 生成專案加密鑰匙
    ```bash
    # !在工作區終端機內
    php artisan key:generate
    php artisan jwt:secret
    ```
8. 導入資料表欄位
    ```bash
    # !在工作區終端機內
    php artisan migrate
    ```
9. 導入預設資料
    ```bash
    # 在工作區終端機內
    php artisan db:seed
    ```
10. 導入測試資料(可選)
    ```bash
    # !在工作區終端機內
    php artisan db:seed --class=TestDataSeeder
    ```
11. 載入其他 laravel-admin 插件
    ```bash
    # !在工作區終端機內
    php artisan admin:import scheduling # 管理任務排程
    php artisan admin:import log-viewer # 管理Log
    ```
12. 打開網頁
    - 前台 [http://localhost](http://localhost)
    - 後台 [http://localhost/admin](http://localhost/admin)
