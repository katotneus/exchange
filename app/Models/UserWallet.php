<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserWallet extends Model
{
    public function asset()
    {
        return $this->belongsTo(Asset::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function available()
    {
        $total = $this->total;
        $freeze = $this->freeze;
        return $total - $freeze >= 0 ? $total - $freeze : 0;
    }
}
