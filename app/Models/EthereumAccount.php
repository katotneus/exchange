<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EthereumAccount extends Model
{
    protected $fillable = [
        'address',
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
