<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;

class APIResponseServiceProvider extends ServiceProvider
{
    const SUCCESS = [
        'status' => 'success',
        'code' => 200,
    ];
    const NO_CONTENT = [
        'status' => 'no content',
        'code' => 204,
    ];
    const NOT_FOUND = [
        'status' => 'not found',
        'code' => 404,
    ];
    const BAD_REQUEST = [
        'status' => 'bad request',
        'code' => 400,
    ];
    const NO_AUTH = [
        'status' => 'unauthenticated',
        'code' => 401,
    ];
    const FORBIDDEN = [
        'status' => 'forbidden',
        'code' => 403,
    ];
    const NO_ENTITY = [
        'status' => 'unprocessable entity',
        'code' => 422,
    ];
    const ERROR = [
        'status' => 'error',
        'code' => 500,
    ];
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('api', function ($data = null, $state = APIResponseServiceProvider::SUCCESS) {
            if (is_integer($state)) {
                $state = APIResponseServiceProvider::code2State($state);
            }
            $status = $state['status'];
            $code = $state['code'];
            $res = ['status' => $status];
            if(!is_null($data)) $res['data'] = $data;

            return Response::json($res, $code);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function code2State($code) {
        switch ($code) {
            case self::SUCCESS['code']:
                return self::SUCCESS;
            case self::NO_CONTENT['code']:
                return self::NO_CONTENT;
            case self::NOT_FOUND['code']:
                return self::NOT_FOUND;
            case self::BAD_REQUEST['code']:
                return self::BAD_REQUEST;
            case self::NO_AUTH['code']:
                return self::NO_AUTH;
            case self::FORBIDDEN['code']:
                return self::FORBIDDEN;
            case self::NO_ENTITY['code']:
                return self::NO_ENTITY;
            default:
                return self::ERROR;
        }
    }
}
