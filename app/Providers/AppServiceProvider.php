<?php

namespace App\Providers;

use App\Models\Order;
use App\Models\Trade;
use App\Observers\OrderObservers;
use App\Observers\TradeObservers;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Order::observe(OrderObservers::class);
        Trade::observe(TradeObservers::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
