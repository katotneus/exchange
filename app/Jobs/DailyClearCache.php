<?php

namespace App\Jobs;

use App\Services\OrderService;
use App\Services\TradeService;
use Illuminate\Support\Facades\Redis;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DailyClearCache implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Order
        $this->clearByNamespace(OrderService::REDIS_NAMESPACE_ORDER_LOGS);

        // Trade
        $this->clearByNamespace(TradeService::REDIS_NAMESPACE_TRADE_LOGS);
        $this->clearByNamespace(TradeService::REDIS_NAMESPACE_TRADE_MAX_PRICE);
        $this->clearByNamespace(TradeService::REDIS_NAMESPACE_TRADE_MIN_PRICE);
    }

    private function clearByNamespace($namespace)
    {
        $keys = Redis::keys($namespace.':*');

        foreach ($keys as $key) {
            Redis::del($key);
        }
    }
}
