<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Log;
use App\Models\Order;
use App\Models\Trade;
use App\Services\TradeService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AfterTrade implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $trade;
    public $trade_service;
    public $asset_target;
    public $asset_price;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Trade $trade)
    {
        $this->trade = $trade;
        $this->asset_target = $trade->orderBuy->assetTarget()->first();
        $this->asset_price = $trade->orderSell->assetPrice()->first();
        $this->trade_service = new TradeService($this->asset_target->id, $this->asset_price->id);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $price = $this->trade->price;

        // 交易價格小於 0，直接停止
        if ($price < 0) return ;

        /**
         * ===============
         * 設定 最高價
         * ===============
         */
        $max_price = $this->trade_service->getMaxPrice();

        // 最高價小於此交易的價格
        if ($max_price < $price) {
            $this->trade_service->serMaxPrice($price);
        }

        /**
         * ===============
         * 設定 最低價
         * ===============
         */
        $min_price = $this->trade_service->getMinPrice();

        // 最低價大於此交易的價格
        if ($min_price > $price) {
            $this->trade_service->setMinPrice($price);
        }

        /**
         * ===============
         * 設定 紀錄
         * ===============
         */
        $this->trade_service->setLog($this->trade);
    }
}