<?php

namespace App\Http\Controllers\API;

use JWTAuth;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\APIResponseServiceProvider as APIResponse;
use Mockery\Exception;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.refresh')->only('refresh');
    }
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string',
        ]);
        $credentials = $request->only('email', 'password');
        try {
            $token = JWTAuth::attempt($credentials);
        } catch (JWTException $e) {
            return response()->api('Could not create token', APIResponse::ERROR);
        }
        if (!$token) return response()->api('Invalid credentials', APIResponse::NO_AUTH);
        try {
            $user = User::where('email', $credentials['email'])->firstOrFail();
        } catch (Exception $e) {
            return response()->api('Could not find user', APIResponse::ERROR);
        }
        if (!$user) return response()->api('Not found user data', APIResponse::NOT_FOUND);

        return response()->api(array_merge($user->toArray(), compact('token')));
    }
    public function refresh()
    {
        return response()->api();
    }
}
