<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Auth;
use Log;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $profile = $user->profile()->first();
        
        return response()->api($profile);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            'phone' => 'numeric',
        ]);
        $data = $request->only([
            'phone',
        ]);
        $profile = $user->profile()->updateOrCreate([], $data);
        return response()->api($profile);
    }
}
