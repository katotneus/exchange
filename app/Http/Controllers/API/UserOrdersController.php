<?php

namespace App\Http\Controllers\API;

use Log;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\APIResponseServiceProvider as APIResponse;

class UserOrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $user = auth()->user();
        // $orders = $user->orders()->get();
        // return response()->api($orders);
        throw new Error();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'type' => 'required|in:buy,sell',
            'asset_target' => 'required|exists:assets,id',
            'asset_price' => 'required|exists:assets,id',
            'amount' => 'required|numeric|min:0',
            'price' => 'required|numeric|min:0',
        ]);
        $user = auth()->user();
        if ($request->type == Order::TYPE__BUY) {
            $wallet = $user->wallets()->where([
                'asset_id' => $request->asset_price,
            ])->firstOrFail();
            $spend = $request->price * $request->amount;
        } elseif ($request->type == Order::TYPE__SELL) {
            $wallet = $user->wallets()->where([
                'asset_id' => $request->asset_target,
            ])->firstOrFail();
            $spend = $request->amount;
        }
        $is_enough = $wallet->available() - $spend > 0;
        $asset_name = $wallet->asset()->first()->name ?: '';
        if (!$is_enough) return response()->api([
            'error' => 'Fail enough balance',
            'messages' => "$asset_name, Not enough available balance",
        ], APIResponse::FORBIDDEN);

        $order = new Order;
        $order->type = $request->type;
        $order->user_id = $user->id;
        $order->asset_id_target = $request->asset_target;
        $order->asset_id_price = $request->asset_price;
        $order->amount = $request->amount;
        $order->price = $request->price;
        $order->save();

        return response()->api($order);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth()->user();
        $order = $user->orders()->find($id);
        if (!$order) return response()->api(null, APIResponse::NOT_FOUND);
        return response()->api($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = auth()->user();
        $order = $user->orders()->findOrFail($id);
        if ($order->status != Order::STATUS__CANCEL) {
            $order->status = Order::STATUS__CANCEL;
            $order->save();
            
            return response()->api($order);
        }
        return response()->api(null, APIResponse::FORBIDDEN);
    }
}
