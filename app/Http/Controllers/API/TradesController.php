<?php

namespace App\Http\Controllers\API;

use App\Models\Trade;
use App\Models\Order;
use App\Services\TradeService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TradesController extends Controller
{
    public function logs(Request $request)
    {
        $request->validate([
            'target_id' => 'required',
            'price_id' => 'required',
        ]);
        
        $trade_service = new TradeService($request->target_id, $request->price_id);
        $trades = $trade_service->getLogs();

        return response()->api($trades);
    }

    public function price(Request $request)
    {
        $request->validate([
            'target_id' => 'required',
            'price_id' => 'required',
            'type' => 'required|in:max,min',
        ]);

        $trade_service = new TradeService($request->target_id, $request->price_id);

        switch ($request->type) {
            case 'max':
                return response()->api($trade_service->getMaxPrice());
            case 'min':
                return response()->api($trade_service->getMinPrice());
        }
    }

    public function show($id)
    {
        $trade = Trade::with([
            'orderBuy.assetTarget', 'orderBuy.user',
            'orderSell.assetPrice', 'orderSell.user',
        ])->find($id);

        return response()->api($trade);
    }
}
