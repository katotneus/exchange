<?php

namespace App\Console;

use App\Models\Order;
use App\Jobs\MatchOrder;
use App\Jobs\DailyClearCache;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // // 定時執行撮合
        // $schedule->call(function () {
        //     dispatch(new MatchOrder)->onQueue('trades');
        // })->name('App\Jobs\MatchOrder')->everyTenMinutes(); 

        // // 每天午夜清除 Redis 的場上快取
        // $schedule->call(function () {
        //    dispatch(new DailyClearCache);
        // })->name('App\Jobs\DailyClearCache')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
