<?php

namespace App\Admin\Controllers;

use App\Models\Asset;
use App\Models\AssetRule;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class AssetRuleController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Asset Rule');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit Asset Rule');
            $content->description('');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Create Asset Rule');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(AssetRule::class, function (Grid $grid) {
            $grid->column('asset_id', 'Asset ID')->sortable();
            $grid->column('source.name', 'Asset Name');
            $grid->column('asset_id_target', 'Target Asset ID');
            $grid->column('target.name', 'Target Asset Name');
            $grid->column('passed', 'Can Buy/Sell?')->switch();

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(AssetRule::class, function (Form $form) {
            $form->display('id', 'ID');
            $form->select('asset_id', 'Source Asset')->options(function ($id) {
                $asset = Asset::find($id);

                if ($asset) {
                    return [$asset->id => $asset->name];
                }
            })->ajax('/admin/api/assets');
            $form->select('asset_id_target', 'Target Asset')->options(function ($id) {
                $asset = Asset::find($id);

                if ($asset) {
                    return [$asset->id => $asset->name];
                }
            })->ajax('/admin/api/assets');
            $form->switch('passed', 'Can Trade?');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
