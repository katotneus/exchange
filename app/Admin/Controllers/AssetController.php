<?php

namespace App\Admin\Controllers;

use App\Models\Asset;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class AssetController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Asset');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit Asset');
            $content->description('');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Create Asset');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Asset::class, function (Grid $grid) {
            $grid->column(Asset::ID, 'ID')->sortable();
            $grid->column(Asset::NAME, 'Name');
            $grid->column(Asset::SYMBOL, 'Symbol')->label();
            $grid->column(Asset::DECIMAL, 'Decimal');
            $grid->column(Asset::TYPE, 'Type')->display(function ($type) {
                switch ($type) {
                    case Asset::TYPE__VIRTUAL:
                    case Asset::TYPE__PHYSICAL:
                        $type_name = ucfirst($type);
                        break;
                    default:
                        $type_name = ucfirst(Asset::TYPE__VIRTUAL);
                }
                return "<span class=\"label label-primary\">$type_name</span>";
            });
            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Asset::class, function (Form $form) {
            $form->display(Asset::ID, 'ID');
            $form->text(Asset::NAME, 'Name');
            $form->text(Asset::SYMBOL, 'Symbol');
            $form->number(Asset::DECIMAL, 'Decimal');
            $form->select(Asset::TYPE, 'Type')->options([
                Asset::TYPE__PHYSICAL => 'Physical',
                Asset::TYPE__VIRTUAL => 'Virtual',
            ]);
            $form->display(Asset::CREATED_AT, 'Created At');
            $form->display(Asset::UPDATED_AT, 'Updated At');
        });
    }
}
