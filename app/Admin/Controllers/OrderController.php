<?php

namespace App\Admin\Controllers;

use App\Models\Order;
use App\Models\Asset;
use App\Models\User;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\MessageBag;

class OrderController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Order');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit Order');
            $content->description('');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Create Order');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Order::class, function (Grid $grid) {
            $grid->column(Order::ID, 'ID')->sortable();
            $grid->column(Order::USER_ID, 'User ID');
            $grid->column('user.name', 'User Name');
            $grid->column(Order::ASSET_ID_TARGET, 'Target Asset ID');
            $grid->column('assetTarget.name', 'Target Asset Name');
            $grid->column(Order::ASSET_ID_PRICE, 'Price Asset ID');
            $grid->column('assetPrice.name', 'Price Asset Name');
            $grid->column(Order::AMOUNT, 'Amount');
            $grid->column(Order::PRICE, 'Price');
            $grid->column(Order::TYPE, 'Type')->display(function ($type) {
                switch ($type) {
                    case Order::TYPE__BUY:
                    case Order::TYPE__SELL:
                        $type_name = strtoupper($type);
                        break;
                    default:
                        $type_name = 'NONE';
                }
                return "<span class=\"label label-info\">$type_name</span>";
            });
            $grid->column(Order::STATUS, 'Status')->display(function ($status) {
                switch ($status) {
                    case Order::STATUS__SUCCESS:
                    case Order::STATUS__ERROR:
                    case Order::STATUS__CANCEL:
                    case Order::STATUS__PENGIND:
                        $status_name = strtoupper($status);
                        break;
                    default:
                        $status_name = 'NONE';
                }

                switch ($status) {
                    case Order::STATUS__SUCCESS:
                        $status_type = 'success';
                        break;
                    case Order::STATUS__ERROR:
                        $status_type = 'danger';
                        break;
                    case Order::STATUS__CANCEL:
                        $status_type = 'warning';
                        break;
                    case Order::STATUS__PENGIND:
                        $status_type = 'primary';
                        break;
                    default:
                        $status_type = 'default';
                        break;
                }
                return "<span class=\"label label-$status_type\">$status_name</span>";
            });

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Order::class, function (Form $form) {
            $form->display(Order::ID, 'ID');
            $form->radio(Order::TYPE)->options([
                Order::TYPE__BUY => 'Buy', Order::TYPE__SELL => 'Sell'
            ])->default(Order::TYPE__BUY);
            $form->select(Order::USER_ID, 'User')->options(function ($id) {
                $user = User::find($id);

                if ($user) {
                    return [$user->id => $user->name];
                }
            })->ajax('/admin/api/users');
            $form->select(Order::ASSET_ID_TARGET, 'Target Asset')->options(function ($id) {
                $asset = Asset::find($id);

                if ($asset) {
                    return [$asset->id => $asset->name];
                }
            })->ajax('/admin/api/assets');
            $form->select(Order::ASSET_ID_PRICE, 'Price Asset')->options(function ($id) {
                $asset = Asset::find($id);

                if ($asset) {
                    return [$asset->id => $asset->name];
                }
            })->ajax('/admin/api/assets');
            $form->number(Order::AMOUNT, 'Amount');
            $form->number(Order::PRICE, 'Price');
            $form->select(Order::STATUS, 'Status')->options([
                Order::STATUS__SUCCESS => 'Success',
                Order::STATUS__ERROR => 'Error',
                Order::STATUS__CANCEL => 'Cancel',
                Order::STATUS__PENGIND => 'Pending',
            ])->default(Order::STATUS__PENGIND);

            $form->display(Order::CREATED_AT, 'Created At');
            $form->display(Order::UPDATED_AT, 'Updated At');

            $form->saving(function (Form $form) {
                $user = User::find($form->user_id);
                $is_enough = false;
                $spend = 0;

                if ($form->type == Order::TYPE__BUY) {
                    $wallet = $user->wallets()->where([
                        'asset_id' => $form->asset_id_price,
                    ])->firstOrFail();
                    $spend = $form->price * $form->amount;
                } elseif ($form->type == Order::TYPE__SELL) {
                    $wallet = $user->wallets()->where([
                        'asset_id' => $form->asset_id_target,
                        ])->firstOrFail();
                    $spend = $form->amount;
                }
                $is_enough = $wallet->available() - $spend > 0;
                $asset_name = $wallet->asset()->first()->name ?: '';
                if (!$is_enough) {
                    $error = new MessageBag([
                        'title'   => 'Fail enough balance',
                        'message' => "$asset_name, Not enough available balance",
                    ]);
                
                    return back()->with(compact('error'));
                }
            });
        });
    }
}
