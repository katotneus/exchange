<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\UserProfile;
use App\Models\UserWallet;
use App\Models\Asset;
use App\Models\AssetRule;
use App\Models\BankAccount;
use App\Models\EthereumAccount;
use App\Models\Order;
use App\Models\Trade;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->create_user();
        $this->create_asset();
        $this->create_asset_wallet();
        $this->create_order();
    }

    private function create_user()
    {
        $user = User::create([
            'name' => 'test1',
            'email' => 'test1@example.com',
            'password' => 'test1234',
        ]);
        $user_profile = UserProfile::create([
            'user_id' => $user->id,
            'phone' => '123456789',
        ]);
        $bank_profile = BankAccount::create([
            'user_id' => $user->id,
            'account' => 'bank123456789',
            'name' => 'TEST Bank',
            'bank_code' => 'B11123',
            'country_code' => 'TW',
        ]);
        $ethereum_profile = EthereumAccount::create([
            'user_id' => $user->id,
            'address' => '0x123456789',
        ]);
    }

    private function create_asset()
    {
        $asset1 = Asset::create([
            'name' => 'TEST Asset 001',
            'symbol' => 'TA1',
            'type' => 'virtual',
            'decimal' => 0,
        ]);
        $asset2 = Asset::create([
            'name' => 'TEST Asset 002',
            'symbol' => 'TA2',
            'type' => 'virtual',
            'decimal' => 0,
        ]);
        $rule = AssetRule::create([
            'asset_id' => $asset1->id,
            'asset_id_target' => $asset2->id,
            'passed' => true,
        ]);
    }

    private function create_asset_wallet()
    {
        $user = User::where([
            'email' => 'test1@example.com',
        ])->first();
        $asset1 = Asset::where([
            'symbol' => 'TA1',
        ])->first();
        $asset2 = Asset::where([
            'symbol' => 'TA2',
        ])->first();
        $asset1_wallet = UserWallet::create([
            'user_id' => $user->id,
            'asset_id' => $asset1->id,
            'total' => 100000,
            'freeze' => 0,
        ]);
        $asset2_wallet = UserWallet::create([
            'user_id' => $user->id,
            'asset_id' => $asset2->id,
            'total' => 200000,
            'freeze' => 0,
        ]);
    }

    private function create_order()
    {
        $user = User::where([
            'email' => 'test1@example.com',
        ])->first();
        $asset1 = Asset::where([
            'symbol' => 'TA1',
        ])->first();
        $asset2 = Asset::where([
            'symbol' => 'TA2',
        ])->first();
        $asset1_wallet = $user->wallet()->where([
            'asset_id' => $asset1->id,
        ])->first();
        $asset2_wallet = $user->wallet()->where([
            'asset_id' => $asset2->id,
        ])->first();
        $buy1 = Order::create([
            'user_id' => $user->id,
            'asset_id_target' => $asset1->id,
            'asset_id_price' => $asset2->id,
            'type' => 'buy',
            'amount' => 10,
            'price' => 100,
        ]);
        $sell1 = Order::create([
            'user_id' => $user->id,
            'asset_id_target' => $asset1->id,
            'asset_id_price' => $asset2->id,
            'type' => 'sell',
            'amount' => 5,
            'price' => 200,
        ]);
        $sell2 = Order::create([
            'user_id' => $user->id,
            'asset_id_target' => $asset1->id,
            'asset_id_price' => $asset2->id,
            'type' => 'sell',
            'amount' => 5,
            'price' => 95,
        ]);
    }
}
